#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  Copyright (c) 2022 Oliver Borm.
#  
#  This program is free software: you can redistribute it and/or modify  
#  it under the terms of the GNU General Public License as published by  
#  the Free Software Foundation, version 3.
# 
#  This program is distributed in the hope that it will be useful, but 
#  WITHOUT ANY WARRANTY; without even the implied warranty of 
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
#  General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License 
#  along with this program. If not, see <http://www.gnu.org/licenses/>.
#
"""
Proof-of-concept simulation of an internal combustion engine base on a 
perferctly stirred reactor.

Note that this example uses numerous simplifying assumptions and
thus serves for illustration purposes only.

isentropic compression and expansion

The simulation works for three different fuels:
    - Diesel
    - Syngas (H2:CO in a 2:1 mixture)
    - Methanol

For Syngas and Methanol the mixture is ignited with a pilot fuel spike. The 
default pilot fuel is 'H' for 2.5° crank angle.

Diesel:
    The simulation uses n-Dodecane as fuel, which is injected close to top dead
    center. 

For Syngas and Methanol, there are two fuel injection timing options:
    - Otto-type cycle principle: injection close to after inlet valve closure 
    at 220° (default). Combustion takes place homogeniously in whole 
    in-cylinder volume oncurrently. Whereas in reality the flame would 
    propagate from the spark plug throught the cylinder volume.
    - Otto-type cycle principle: injection close to top dead centre at 350° 
    (default). Combustion takes place homogeniously in whole in-cylinder volume
    under lean conditions (mixed=burnt). Whereas in reality the flame would
    burn as diffusion flame with a local phi=1 flame front.

The heat of evaporation under standard conditions is 256~kJ/kg for dodecane and
1165~kJ/kg for methanol. The critical pressure for dodecane is 18 bar, whereas
for methanol it's 81 bar. With typical compressions ratios in diesel engines,
the diesel injection happens at supercritical conditions. Thus, the fuel
droplets are not evaporated through a 2-phase region.
Due to the higher critical pressure of methanol, a pressure dependent heat of
evaporation function was implemented. The heat of evaporation is then applied
as time-dependent external heat sink to the cylinder wall. It updates the wall
heat flux for each external time step dt (=1° CA per default), and kept it
constant during the advance() call. It takes into account the in-cylinder
temperature and mass, and calculates the maximum fuel which can be evaporated.
Thus it may delay the fuel evaporation until enough energy is available during
compression cycle. As the fuel injection itself is modelled as gas injection 
this is the closest modelling you can get within this simplified framework. The
heat and mass balance is not fully conservative in case of a delayed
evaporation, because all the fuel is already considered gaseous, which effects
eventually in-cylinder density, and as such pressure and temperature.
A time dependent wall heat source affects the numerical stability during
compression and numerical tolerance parameters needed to be relaxed.

The Syngas fuel composition reflects the methanol decomposition (MD) described:
"Influence of methanol reformate injection strategy on performance, available
exhaust gas enthalpy and emissions of a direct-injection spark ignition engine"
A. Poran, L. Tartakovsky
International Journal of Hydrogen Energy, ISSN: 0360-3199, Vol: 42, Issue: 23,
Page: 15652-15668, 2017
https://doi.org/10.1016/j.ijhydene.2017.05.056

The reduced methanol rection mechanism is from:
"Reduced Kinetic Mechanism for Methanol Combustion in Spark-Ignition Engines"
Christoffer Pichler and Elna J. K. Nilsson
Energy Fuels 2018, 32, 12, 12805–12813
Publication Date: October 30, 2018
https://doi.org/10.1021/acs.energyfuels.8b02136

The detailed methanol reaction mechanism is from:
"A Detailed Chemical Kinetic Modeling, Ignition Delay time and Jet-Stirred
Reactor Study of Methanol Oxidation"
U. Burke, W.K. Metcalfe, S.M. Burke, K.A. Heufer, P. Dagaut, H.J. Curran
Combustion and Flame (2016) 165 125–136.
http://dx.doi.org/10.1016/j.combustflame.2015.11.004
http://c3.nuigalway.ie/combustionchemistrycentre/mechanismdownloads/

Requires: cantera >= 2.5.0, scipy >= 0.19, matplotlib >= 2.0, reportlab (only for pdf export)
"""

import cantera as ct
import numpy as np

from scipy.integrate import trapz
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

#########################################################################
# Input Parameters
#########################################################################

# fuel, injection timing, reaction mechanism, kinetics type and compositions

fuel = "liquid"
injTime = 1

export = True
plot = True
verbose = 1

# Simulation time and parameters
delta_T_max = 20.
rtol = 1.e-12
atol = 1.e-16

sim_n_revolutions = 8
frequency = 3000. / 60.  # engine speed [1/s] (3000 rpm)
dt = 1. / (360 * frequency) ## set delta t to 1° crank angle

V_H = .5e-3  # displaced volume [m**3]
epsilon = 18.  # compression ratio [-]
d_piston = 0.083  # piston diameter [m]

comp_air = 'o2:1, n2:3.76'

T_inlet = 300.  # K
p_inlet = 1.3e5  # Pa
comp_inlet = comp_air

ignition_angle = np.deg2rad(350)
igniter_delta = np.deg2rad(2.5)
fwhm = 0.2

if fuel == "diesel":
    reaction_mechanism = 'nDodecane_Reitz.yaml'
    phase_name = 'nDodecane_IG'
    fuel_name = 'c12h26'
    comp_fuel = fuel_name+':1'
    epsilon = 18.  # compression ratio [-]
    in_open = 350
    ## that's only an lambda approximate value
    lam_da = 1.063
    fuel_mass_in = 3.622662648662489e-5/lam_da
    injector_delta = 15.
    igniter_amplitude = 0.0
    ignFuel = 'H'
    
    ### the latent heat of evaporation is also a function of pressure in its own, and thus depends on the in cylinder pressure, during injection
    ## https://webbook.nist.gov/cgi/cbook.cgi?Name=dodecane&Units=SI&cTP=on
    ## critical pressure of dodecane is 18 bar, critical Temperature 385°C=658.15 K
    ## as injection typical happens above critical point, there's no evaporation enthalpy to be considered for dodecane
    qevap = 0 #-256e3 #[J/kg] 251 for kerosene; 256 for Dodecane
    
    LHV = 42.6e6 ##[J/kg]
    
elif fuel == "gas":
    reaction_mechanism = 'gri30.yaml'
    phase_name = 'gri30'
    fuel_name = 'H2'
    comp_fuel = 'CO:1,H2:2'
    
    lam_da = 1.05 #*1.2023115577889447 ## LHV_ratio_syngas_meoh
    fuel_mass_in = 1.4877169e-4/lam_da/2
    
    if injTime == 0:
        in_open = 220
        injector_delta = 30.
        igniter_amplitude = fuel_mass_in*lam_da/igniter_delta*(2*np.pi)/150
#        igniter_amplitude = 0
    elif injTime == 1:
        in_open = 350
        injector_delta = 15.
        igniter_amplitude = fuel_mass_in*lam_da/igniter_delta*(2*np.pi)/8

    qevap = 0 #[J/kg] 
    LHV = 19.9e6 ## 23.926e6 ## [J/kg]
    
    ignFuel = 'H'
    
    # turbocharger temperature, pressure, and composition
#    T_inlet = 310.  # K
#    p_inlet = 2.25e5  # Pa
    
elif fuel == "liquid":
#    reaction_mechanism = 'gri30.yaml'
#    phase_name = 'gri30'
#    reaction_mechanism = 'AramcoMech2.0.yaml' ## doesn't work with evaporation
    ## reduced methanol reaction mechanism
    reaction_mechanism = 'ef8b02136_si_002.yaml'
    ## detailed methanol reaction mechanism
#    reaction_mechanism = 'Methanol_2016_mech.yaml'
    phase_name = 'gas'
#    reaction_mechanism = 'nDodecane_Reitz.yaml'
#    phase_name = 'nDodecane_IG'
    fuel_name = 'CH3OH'
    comp_fuel = fuel_name+':1'
    
    lam_da = 1.05
    fuel_mass_in = 1.4877169e-4/lam_da/2
    
    if injTime == 0:
        in_open = 220
        injector_delta = 30.
        igniter_amplitude = fuel_mass_in*lam_da/igniter_delta*(2*np.pi)/2.5
        ignFuel = 'H'
        
        rtol = 1.e-8
        atol = 1.e-16
        
    elif injTime == 1:
        in_open = 350
        injector_delta = 15.
        igniter_amplitude = fuel_mass_in*lam_da/igniter_delta*(2*np.pi)/8
        ignFuel = 'H'
#        ignFuel = 'c12h26'
        
        rtol = 1.e-10
        atol = 1.e-16

    ## https://webbook.nist.gov/cgi/cbook.cgi?ID=C67561&Mask=4#Thermo-Phase
    ##critical pressure 81 bar, critical T 513.K
    qevap = -1165e3 #[J/kg]
    LHV = 19.9e6 ##[J/kg]

    # turbocharger temperature, pressure, and composition
#    T_inlet = 310.  # K
#    p_inlet = 2.25e5  # Pa

## turn off spark ignition
#igniter_amplitude = 0

# outlet pressure
p_outlet = 1.2e5  # Pa

# fuel properties (gaseous!)
T_injector = 300.  # K
p_injector = 1600e5  # Pa
comp_injector = comp_fuel

# ambient properties
T_ambient = 300.  # K
p_ambient = 1e5  # Pa
comp_ambient = comp_air

# Inlet valve friction coefficient, open and close timings
inlet_valve_coeff = 1.e-6
inlet_open = np.deg2rad(-18.)
inlet_close = np.deg2rad(198.)

# Outlet valve friction coefficient, open and close timings
outlet_valve_coeff = 1.e-6
outlet_open = np.deg2rad(522.)
outlet_close = np.deg2rad(18.)

# Fuel mass, injector open and close timings
injector_open = np.deg2rad(in_open)
injector_close = np.deg2rad(in_open+injector_delta)
injector_mass = fuel_mass_in  # kg

#####################################################################
# Set up IC engine Parameters and Functions
#####################################################################

V_oT = V_H / (epsilon - 1.)
A_piston = .25 * np.pi * d_piston ** 2
stroke = V_H / A_piston


def crank_angle(t):
    """Convert time to crank angle"""
    return np.remainder(2 * np.pi * frequency * t, 4 * np.pi)


def piston_speed(t):
    """Approximate piston speed with sinusoidal velocity profile"""
    return - stroke / 2 * 2 * np.pi * frequency * np.sin(crank_angle(t))


#####################################################################
# Set up Reactor Network
#####################################################################

# load reaction mechanism
gas = ct.Solution(reaction_mechanism, phase_name)

# calculate the stoichiometric Air-Fuel-Ratrio
gas.set_equivalence_ratio(1.0, comp_fuel, comp_inlet)
Y_fuel = 0.0
fuelc = comp_fuel.split(",")
for fs in fuelc:
    Y_fuel += gas[fs.split(":")[0]].Y[0]
    
AFR_st = (1-Y_fuel)/Y_fuel

# define initial state and set up reactor
gas.TPX = T_inlet, p_inlet, comp_inlet
cyl = ct.IdealGasReactor(gas)
cyl.volume = V_oT

# define inlet state
gas.TPX = T_inlet, p_inlet, comp_inlet
inlet = ct.Reservoir(gas)

# inlet valve
inlet_valve = ct.Valve(inlet, cyl)
inlet_delta = np.mod(inlet_close - inlet_open, 4*np.pi)
inlet_valve.valve_coeff = inlet_valve_coeff
inlet_valve.set_time_function(
    lambda t: np.mod(crank_angle(t) - inlet_open, 4*np.pi) < inlet_delta)

# define injector state (gaseous!)
gas.TPX = T_injector, p_injector, comp_injector
injector = ct.Reservoir(gas)
M_fuel = gas.mean_molecular_weight ## kg/kmol

# injector is modeled as a mass flow controller
injector_mfc = ct.MassFlowController(injector, cyl)
injector_delta_rad = np.mod(injector_close - injector_open, 4*np.pi)
injector_t_open = (injector_close - injector_open) / 2. / np.pi / frequency
injector_mfc.mass_flow_coeff = injector_mass / injector_t_open
injector_mfc.set_time_function(
    lambda t: np.mod(crank_angle(t) - injector_open, 4 * np.pi) < injector_delta_rad)

# define outlet pressure (temperature and composition don't matter)
gas.TPX = T_ambient, p_outlet, comp_ambient
outlet = ct.Reservoir(gas)

# outlet valve
outlet_valve = ct.Valve(cyl, outlet)
outlet_delta = np.mod(outlet_close - outlet_open, 4*np.pi)
outlet_valve.valve_coeff = outlet_valve_coeff
outlet_valve.set_time_function(
    lambda t: np.mod(crank_angle(t) - outlet_open, 4*np.pi) < outlet_delta)

# define ambient pressure (temperature and composition don't matter)
gas.TPX = T_ambient, p_ambient, comp_ambient
ambient_air = ct.Reservoir(gas)

def evap_T_MeOH(tP):
    """
        calculate the evaporation temperature as function of the vapour pressure for methanol
        source: https://webbook.nist.gov/cgi/cbook.cgi?ID=C67561&Mask=4#Thermo-Phase
    """
    P = tP*1e-5 ## convert to bar
    if P >= 1.7091124656074195 and P <= 81.: #74.70269554659326:        
        A=5.15853
        B=1569.613
        C=-34.846
        return B/(A-np.log10(P))-C
    
    elif P >= 0.09840393278343013 and P < 1.7091124656074195:
        A=5.20409
        B=1581.341
        C=-33.50
        return B/(A-np.log10(P))-C
    else:
        return 512.6

def evap_MeOH(T):
    """
        calculate the heat of evaporation as function of the temperature for methanol
        source: https://webbook.nist.gov/cgi/cbook.cgi?ID=C67561&Mask=4#Thermo-Phase
    """
    A = 45.3
    alpha = -0.31
    beta = 0.4241
    Tc = 512.6
    Tr =  T / Tc
    if Tr <= 1 and Tr > 0.5620366757705814:
        ## ΔvapH = A exp(-αTr) (1 − Tr)**β within 298. - 477. in (kJ/mol)
        ## A = 45.3; α = -0.31, β=0.4241, Tc = 512.6
        return A*np.exp(-alpha*Tr)*(1-Tr)**beta ## kJ/mol
    else:
        return 0.0

mFuel_res = 0.0
mFuel_complete = 0.0
q_all = 0.0
## reference evaporation at constant pressure and constant heat of evaporation
q0_ref = qevap*injector_mass #/ (injector_t_open)
q0 = 0.0

def set_q_evap(thermo,m_cyl,myTime):
    """
        set the heat of evaporation per timestep for methanol
        preserve energy and make sure temperature doesn't drop below evaporation 
        temperature at given pressure
        delay heat release from evaporation until temperature high enough
        
        fuel droplets will have the same pressure as in-cylinder after injection,
        but their temperature is lower. They still need to heat up and and
        eventually evaporate.
        Assumption: in-cylinder pressure in each time step == vapour pressure
        
        inconsitent with massflow, as all fuel is injected as gas, and thus also
        affects the density and cp/cv of gas, as all fuel is already in gas state
        best/good approximation which can done with a perfectly stirred reactor
    """
    
    ca = crank_angle(myTime)
    
    global mFuel_res
    global mFuel_complete
    global q_all
    global q0
    
    ## injected fuel per timestep
    dm = injector_mass/injector_t_open*dt
    
    if (np.mod(ca-injector_open, 4*np.pi) < injector_delta_rad):
        mFuel_complete += dm
        if mFuel_complete > injector_mass:
            if verbose > 2: print("mFuel_complete too big; ca =",np.rad2deg(ca),mFuel_complete,injector_mass)
    
        if mFuel_complete <= injector_mass:
            mFuel_res += dm
#            print(np.rad2deg(ca),mFuel_complete,mFuel_res)
    
    ## reset mFuel_complete after one cycle
    if (np.mod(ca-outlet_open, 4*np.pi) > injector_delta_rad):
        if mFuel_res > 0.0: "WARNING: not everything evaporated"
#        print("reset", ca)
        mFuel_complete = 0.0
    
    ## minimum temperatue difference between in-cylinder temperature and fuel droplet temperature
    dTemp = 10
    
    if fuel == "liquid" and mFuel_res > 0.0:
        Tevap = evap_T_MeOH(thermo.P)
        ## heat of evaporation only below critical point
        if Tevap < 512.6:
            if thermo.T > Tevap+dTemp:
                q_evap = evap_MeOH(Tevap)*1e6/M_fuel ## kJ/mol/kg/kmol*1e6 = MJ/kg*1e6 = J/kg
                ## fuel which can be evaporated
                mF_evap = m_cyl*thermo.cv_mass*(thermo.T-Tevap-dTemp)/(q_evap+thermo.cv_mass*(thermo.T-Tevap-dTemp))
                ## fuel which will be evaporated including residual fuel from former time steps
                mres0 = mFuel_res
                mF_all = min(max(mF_evap,0.0),mFuel_res)
                mFuel_res -= max(mF_all,0.0)
                
                q0 = -q_evap*mF_all/dt ##J/kg*kg/(s) = W
                ## just check that the overall heat release is not much bigger than reference one
                q_all += q0*dt
                
                if verbose > 2:
                    print("P {0:.4g}".format(thermo.P/1e5), "T {0:.1f}".format(thermo.T),"Tevap {0:.1f}".format(Tevap), "q_evap {0:.4g}".format(q_evap))
                    print("CA {0:.4g}, mF_evap {1:.4g}".format(np.rad2deg(ca),mF_evap), "mF_res {0:.4g}".format(mFuel_res), "mFuel_res0 {0:.4g}".format(mres0) )
                    
                    print("q0 {0:.4g}, q_evap {1:.4g}, mF_all {2:.4g}".format(q0,q_evap,mF_all)) ##,q_fuel/(dt*A_piston)
                    
                    print("qall, q0_ref", q_all,q0_ref*sim_n_revolutions*0.5)
                
            else:
                if verbose > 2: print("CA {0:.4g}, thermo.T < Tevap".format(np.rad2deg(ca)))
                q0 = 0.0
        else:
            ## supercritical condition, where the incylinder pressure exceeds the critical pressure
            if thermo.P/1e5 < 81.:
                print("WARNING Tevap > Tc", "mFuel_res {0:.4g}".format(mFuel_res),"P {0:.4g}".format(thermo.P/1e5))
            mFuel_res = 0.0
            q0 = 0.0
    else:
        q0 = 0.0

def get_q0(t):
    """return a constant evaporation heat power during advancing the simulation"""
    if abs(q0) > 0 and verbose > 3: print("get q0 {0:.4g}, q0_ref {1:.4g}, CA {2:.5f}, t {3:.6g}".format(q0/(A_piston), qevap*injector_mass / (injector_t_open*A_piston), np.rad2deg(crank_angle(t)), t) )
    return q0/(A_piston) ##J/kg*kg/(s*m²) = W/m²

## piston is modeled as a moving wall --> isentropic compression and expansion
piston = ct.Wall(ambient_air, cyl)
piston.area = A_piston
piston.set_velocity(piston_speed)
## as the fuel injection is modelled as gas injection, the latent heat of evaporation must be modelled as external heat loss in order to reduce the in-cylinder temperature
piston.set_heat_flux(
    lambda t: get_q0(t)
    ) 

# The igniter will use a Gaussian time-dependent mass flow rate.
gas.TPX = 2000.0, ct.one_atm*1000, ignFuel+':1.0'
igniter = ct.Reservoir(gas)
#igniter_mdot = lambda t: amplitude * np.exp(-(t-t0)**2 * 4 * np.log(2) / fwhm**2)
igniter_mdot = lambda t: igniter_amplitude * np.exp(-(np.mod(crank_angle(t) - ignition_angle, 4*np.pi)/(4*np.pi))**2 / fwhm**2)*(np.mod(crank_angle(t) - ignition_angle, 4*np.pi) < igniter_delta)
igniter_mfc = ct.MassFlowController(igniter, cyl, mdot=igniter_mdot)

# create a reactor network containing the cylinder and limit advance step
sim = ct.ReactorNet([cyl])
sim.rtol, sim.atol = rtol, atol
cyl.set_advance_limit('temperature', delta_T_max)
sim.verbose = False

#####################################################################
# Run Simulation
#####################################################################

# set up output data arrays
states = ct.SolutionArray(
    cyl.thermo,
    extra=('t', 'ca', 'V', 'm', 'mdot_in', 'mdot_out', 'mdot_fuel', 'mdot_ignitor', 'dWv_dt', 'QWall'),
)

# simulate with a maximum resolution of 1 deg crank angle
t_stop = sim_n_revolutions / frequency
while sim.time < t_stop:

    #print("Sim Time:",sim.time)
    ## update evaporation heat for constant dt
    set_q_evap(cyl.thermo,cyl.mass,sim.time)
    
    # perform time integration
    sim.advance(sim.time + dt)

    # calculate results to be stored
    dWv_dt = - (cyl.thermo.P - ambient_air.thermo.P) * A_piston * \
        piston_speed(sim.time)
    
    QWall = piston.qdot(sim.time)
    
    # append output data
    states.append(cyl.thermo.state,
                  t=sim.time, ca=np.rad2deg(crank_angle(sim.time)),
                  V=cyl.volume, m=cyl.mass,
                  mdot_in=inlet_valve.mass_flow_rate,
                  mdot_out=outlet_valve.mass_flow_rate,
                  mdot_fuel=injector_mfc.mass_flow_rate,
                  mdot_ignitor=igniter_mfc.mass_flow_rate,
                  dWv_dt=dWv_dt,
                  QWall=QWall)

#######################################################################
# Write Results in csv file
#######################################################################

t = states.t

# AFR
airIn = trapz(states.mdot_in,t)
airOut = trapz(states.mdot_out,t)
fuelIn = trapz(states.mdot_fuel,t)
ignitorIn = trapz(states.mdot_ignitor,t)
AFR = airIn/(fuelIn)
myLambda = AFR/AFR_st

fName = 'ic_engine_'+fuel+'_fInj-'+str(in_open)+'_fInjDelta-'+str(injector_delta)+'_fIgn-'+str(np.rad2deg(ignition_angle))+'_eps-'+str(epsilon)+"_lambda-"+str(lam_da)

states.write_csv(fName+'.csv', cols=('t', 'ca','T', 'P', 'density', 'cv_mass', 'cp_mass', 's', 'V', 'm', 'mdot_in', 'mdot_out', 'mdot_ignitor', 'mdot_fuel', 'QWall', 'dWv_dt'))


#######################################################################
# Plot Results in matplotlib
#######################################################################

if plot:

    def ca_ticks(t):
        """Helper function converts time to rounded crank angle."""
        return np.round(crank_angle(t) * 180 / np.pi, decimals=1)
    
    
    # pressure and temperature
    fig, ax = plt.subplots(nrows=2)
    ax[0].set_xlim([(sim_n_revolutions-2)/frequency,sim_n_revolutions/frequency])
    ax[0].xaxis.set_major_locator(ticker.LinearLocator(9))
    ax[1].set_xlim([(sim_n_revolutions-2)/frequency,sim_n_revolutions/frequency])
    ax[1].xaxis.set_major_locator(ticker.LinearLocator(9))
    ax[1].xaxis.set_minor_locator(ticker.LinearLocator(73))
    ax[0].plot(t, states.P / 1.e5)
    ax[0].set_ylabel('$p$ [bar]')
    ax[0].set_xlabel(r'$\phi$ [deg]')
    ax[0].set_xticklabels([])
    ax[1].plot(t, states.T)
    ax[1].set_ylabel('$T$ [K]')
    ax[1].set_xlabel(r'$\phi$ [deg]')
    ax[1].set_xticklabels(ca_ticks(ax[1].get_xticks()))
    #plt.savefig("T-p-time_"+fuel+'_eps-'+str(epsilon)+"_f-"+str(f)+"_T_inlet-"+str(T_inlet)+"_in_open-"+str(in_open)+"_lambda-"+str(myLambda)+".pdf")
    plt.show()
    
    # p-V diagram
    fig, ax = plt.subplots()
    ax.plot(states.V[t > 2./frequency] * 1000, states.P[t > 2./frequency] / 1.e5)
    ax.set_xlabel('$V$ [l]')
    ax.set_ylabel('$p$ [bar]')
    plt.show()
    
    # T-S diagram
    fig, ax = plt.subplots()
    ax.plot(states.m[t > 2./frequency] * states.s[t > 2./frequency], states.T[t > 2./frequency])
    ax.set_xlabel('$S$ [J/K]')
    ax.set_ylabel('$T$ [K]')
    plt.show()
    
    # heat of reaction and expansion work
    fig, ax = plt.subplots()
    ax.plot(t, 1.e-3 * states.heat_release_rate*states.V, label=r'$\dot{Q}$')
    ax.plot(t, 1.e-3 * states.dWv_dt, label=r'$\dot{W}_v$')
    ax.set_ylim(-1e2, 1e3)
    ax.legend(loc=0)
    ax.set_ylabel('[kW]')
    ax.set_xlabel(r'$\phi$ [deg]')
    ax.set_xticklabels(ca_ticks(ax.get_xticks()))
    plt.show()
    
    # gas composition
    fig, ax = plt.subplots()
    ax.plot(t, states('o2').X, label='O2')
    ax.plot(t, states('co2').X, label='CO2')
    ax.plot(t, states('co').X, label='CO')
    ax.plot(t, states(fuel_name).X * 10, label='fuel x10')
    ax.legend(loc=0)
    ax.set_ylabel('$X_i$ [-]')
    ax.set_xlabel(r'$\phi$ [deg]')
    ax.set_xticklabels(ca_ticks(ax.get_xticks()))
    plt.show()

######################################################################
# Integral Results
######################################################################

## prepare text output
outputLines = []

outputLines.append("{:45s}{:}".format("Fuel:",fuel))
outputLines.append("{:45s}{:.0f} ".format("Fuel Injection:",injTime))

# heat release
Q0 = trapz(states.heat_release_rate*states.V, t) # total volumetric heat release rate [W/m^3]
outputLines.append('{:45s}{:>4.1f} kW'.format('Heat release rate per cylinder (estimate):',Q0 / t[-1] / 1000.))
Q = fuelIn*LHV
outputLines.append('{:45s}{:>4.1f} kW'.format('Heat release rate (LHV):',Q / t[-1] / 1000.))

# expansion power
W = trapz(states.dWv_dt, t)
Wexp = trapz(np.maximum(states.dWv_dt,0), t)
Wcomp = -trapz(np.minimum(states.dWv_dt,0), t)
outputLines.append('{:45s}{:>4.1f} kW'.format('Power per cylinder (estimate):',W / t[-1] / 1000.))

outputLines.append('{:45s}{:>4.1f} kW'.format('Expansion Power per cylinder:',Wexp / t[-1] / 1000.))
outputLines.append('{:45s}{:>4.1f} kW'.format('Compression Power per cylinder:',Wcomp / t[-1] / 1000.))


outputLines.append('{:45s}{:>4.1f} kJ/kg'.format('Expansion Work:',Wexp/airOut / 1000.))## missing fuel 
outputLines.append('{:45s}{:>4.1f} kJ/kg'.format('Compression Work:',Wcomp/(airIn+fuelIn)  / 1000.))

# efficiency
eta0 = W / Q0
outputLines.append('{:45s}{:>4.1f} %'.format('Efficiency (Heat Release Rate):',eta0 * 100.))

eta = W / Q
outputLines.append('{:45s}{:>4.1f} %'.format('Efficiency (LHV):',eta * 100.))

# Lambda
outputLines.append('{:45s}{:>4.3f}'.format("AFR:",AFR))
outputLines.append('{:45s}{:>4.3f}'.format("Lambda:",AFR/AFR_st))

outputLines.append('{:45s}{:>4.6g} kg'.format('Air Massflow:',airIn))
outputLines.append('{:45s}{:>4.6g} kg {:>4.4g} kJ'.format('Fuel Massflow; Fuel Energy:',fuelIn,fuelIn*LHV/1000))
outputLines.append('{:45s}{:>4.6g} kg {:>4.2g} %'.format('Ignitor Massflow; Percentage of Fuel:',ignitorIn, (ignitorIn/fuelIn)*100))

##
outputLines.append('{:45s}{:>3.0f}°'.format("Start Injection:",in_open))
outputLines.append('{:45s}{:>3.0f}°'.format("Injection Delta:",injector_delta))
outputLines.append('{:45s}{:>3.0f}°'.format("Start Ignition:",np.rad2deg(ignition_angle)))

# CO emissions
MW = states.mean_molecular_weight
CO_emission = trapz(MW * states.mdot_out * states('CO').X[:, 0], t)
CO_emission /= trapz(MW * states.mdot_out, t)
outputLines.append('{:45s}{:>4.1f} ppm'.format('CO emission (estimate):',CO_emission * 1.e6))

outputLines.append('{:45s}{:>4.4g} s'.format('End Simulation time',t[-1]))

## print global values
print(*outputLines, sep = "\n")


if export:
    ### export global values in pdf file
    from reportlab.lib.pagesizes import A4
    from reportlab.platypus import SimpleDocTemplate, Table
    from reportlab.pdfbase import pdfmetrics
    from reportlab.pdfbase.ttfonts import TTFont
     
    pdfmetrics.registerFont(TTFont('Vera', 'Vera.ttf'))
    pdfmetrics.registerFont(TTFont('VeraBd', 'VeraBd.ttf'))
    pdfmetrics.registerFont(TTFont('VeraIt', 'VeraIt.ttf'))
    pdfmetrics.registerFont(TTFont('VeraBI', 'VeraBI.ttf'))
    
    doc = SimpleDocTemplate(fName+".pdf", pagesize=A4)
    flowables = []
    data = []
    for line in outputLines:
        n = 45
        chunks = [line[i:i+n] for i in range(0, len(line), n)]
    #    print(chunks)
        data.append(chunks)
    
    tbl = Table(data)
    flowables.append(tbl)
    doc.build(flowables)



