# cantera-playground

## Description

Loose collection of python scripts for cantera (https://cantera.org/).

This collection consists of:

  - calculation of adiabatic flame temperature
  - general calculation of lower heating value and higher heating value
  - vapour pressure and heat of evaporation for methanol
  - calculation of the thermoydynamic cycle of an internal combustion engine
  - calculation of an internal combustion engine in a perfectly stirred reactor
  - post-processing of various output files of the ice
  - maybe more in the future...

## Installation
You need the python extensions of cantera, plus mostly numpy, scipy, matplotlib, and maybe reportlab installed.

## Contributing
Feel free to use them, play around with them, and maybe improve them, if you find them any handy.

Also feel free to open a bug report, if you think something is wrong or can be improved. I may, or may not fix it ;)

## License
All files are under GNU GPL v3
