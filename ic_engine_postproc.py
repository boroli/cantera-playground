#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  Copyright (c) 2022 Oliver Borm.
#  
#  This program is free software: you can redistribute it and/or modify  
#  it under the terms of the GNU General Public License as published by  
#  the Free Software Foundation, version 3.
# 
#  This program is distributed in the hope that it will be useful, but 
#  WITHOUT ANY WARRANTY; without even the implied warranty of 
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
#  General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License 
#  along with this program. If not, see <http://www.gnu.org/licenses/>.
#
"""
Post-Processing script of various output files from ic_engine script, to
visualise temperature and pressure distribution versus crank angle

@author: Oliver Borm
"""


import matplotlib.pyplot as plt

# Load pandas
import pandas as pd

subdir = "./post-processing"

ign = 1

if ign == 0:
    fInj = "350"
    fIgn = ""
elif ign == 1:
    fInj = "360"
    fIgn = "_fIgn-360.0"

fDiesel_1 = "ic_engine_diesel_fInj-350_fInjDelta-15.0_eps-18.0_lambda-1.063"
dfDiesel_1 = pd.read_csv(subdir+"/"+fDiesel_1+'.csv', index_col=1)

fSynGas_0 = "ic_engine_gas_fInj-220_fInjDelta-30.0"+fIgn+"_eps-18.0_lambda-1.05"
dfSynGas_0 = pd.read_csv(subdir+"/"+fSynGas_0+'.csv', index_col=1)
fSynGas_1 = "ic_engine_gas_fInj-"+fInj+"_fInjDelta-15.0"+fIgn+"_eps-18.0_lambda-1.05"
dfSynGas_1 = pd.read_csv(subdir+"/"+fSynGas_1+'.csv', index_col=1)

fMeOH_0 = "ic_engine_liquid_fInj-220_fInjDelta-30.0"+fIgn+"_eps-18.0_lambda-1.05"
dfMeOH_0 = pd.read_csv(subdir+"/"+fMeOH_0+'.csv', index_col=1)
fMeOH_1 = "ic_engine_liquid_fInj-"+fInj+"_fInjDelta-15.0"+fIgn+"_eps-18.0_lambda-1.05"
dfMeOH_1 = pd.read_csv(subdir+"/"+fMeOH_1+'.csv', index_col=1)

iStart = -700
iEnd = -2

### plot pressure vs crank angle
plt.plot(dfDiesel_1["P"].iloc[iStart:iEnd].index, dfDiesel_1["P"].iloc[iStart:iEnd]*1e-5, label="Diesel - Inj.=350°", color='blue', linestyle='dashed')

plt.plot(dfSynGas_0["P"].iloc[iStart:iEnd].index, dfSynGas_0["P"].iloc[iStart:iEnd]*1e-5, label="Syngas - Inj.=220°", color='red')
plt.plot(dfSynGas_1["P"].iloc[iStart:iEnd].index, dfSynGas_1["P"].iloc[iStart:iEnd]*1e-5, label="Syngas - Inj.="+fInj+"°", color='red', linestyle='dashed')

plt.plot(dfMeOH_0["P"].iloc[iStart:iEnd].index, dfMeOH_0["P"].iloc[iStart:iEnd]*1e-5, label="MeOH - Inj.=220°", color='green')
plt.plot(dfMeOH_1["P"].iloc[iStart:iEnd].index, dfMeOH_1["P"].iloc[iStart:iEnd]*1e-5, label="MeOH - Inj.="+fInj+"°", color='green', linestyle='dashed')

plt.ylabel("Pressure [bar]")
plt.xlabel("Crank Angle [°]")
plt.legend()
plt.grid()

plt.savefig("IC_Engine_pressure_full_fInj-"+fInj+fIgn+".png", dpi=300)

plt.ylim(0,100)
plt.xlim(180,360)
plt.savefig("IC_Engine_pressure_fInj-"+fInj+fIgn+".png", dpi=300)

#plt.show()

plt.ylim(0,200)
plt.xlim(340,380)
plt.savefig("IC_Engine_pressure_ignition_fInj-"+fInj+fIgn+".png", dpi=300)

plt.show()

plt.close("all")

### plot Temperature vs crank angle
dTemp = 273.15

plt.plot(dfDiesel_1["T"].iloc[iStart:iEnd].index, dfDiesel_1["T"].iloc[iStart:iEnd]-dTemp, label="Diesel - Inj.=350°", color='blue', linestyle='dashed')

plt.plot(dfSynGas_0["T"].iloc[iStart:iEnd].index, dfSynGas_0["T"].iloc[iStart:iEnd]-dTemp, label="Syngas - Inj.=220°", color='red')
plt.plot(dfSynGas_1["T"].iloc[iStart:iEnd].index, dfSynGas_1["T"].iloc[iStart:iEnd]-dTemp, label="Syngas - Inj.="+fInj+"°", color='red', linestyle='dashed')

plt.plot(dfMeOH_0["T"].iloc[iStart:iEnd].index, dfMeOH_0["T"].iloc[iStart:iEnd]-dTemp, label="MeOH - Inj.=220°", color='green')
plt.plot(dfMeOH_1["T"].iloc[iStart:iEnd].index, dfMeOH_1["T"].iloc[iStart:iEnd]-dTemp, label="MeOH - Inj.="+fInj+"°", color='green', linestyle='dashed')

plt.ylabel("Temperature [°C]")
plt.xlabel("Crank Angle [°]")
plt.legend()
plt.grid()

plt.savefig("IC_Engine_Temperature_full_fInj-"+fInj+fIgn+".png", dpi=300)

plt.ylim(0,1500)
plt.xlim(180,360)
plt.savefig("IC_Engine_Temperature_fInj-"+fInj+fIgn+".png", dpi=300)

#plt.show()

plt.ylim(250,3000)
plt.xlim(340,380)
plt.savefig("IC_Engine_Temperature_ignition_fInj-"+fInj+fIgn+".png", dpi=300)

plt.show()


