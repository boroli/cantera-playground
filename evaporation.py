#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  Copyright (c) 2022 Oliver Borm.
#  
#  This program is free software: you can redistribute it and/or modify  
#  it under the terms of the GNU General Public License as published by  
#  the Free Software Foundation, version 3.
# 
#  This program is distributed in the hope that it will be useful, but 
#  WITHOUT ANY WARRANTY; without even the implied warranty of 
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
#  General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License 
#  along with this program. If not, see <http://www.gnu.org/licenses/>.
#
"""
Calculate the heat of evapoartion and evaporation temperature as function of
the vapour pressure for methanol

https://webbook.nist.gov/cgi/cbook.cgi?ID=C67561&Mask=4#Thermo-Phase
"""

import numpy as np

from scipy import optimize

##############################################################################

def evap_T_MeOH(P):
    """
        calculate evaporation the temperature as function of the vapour pressure for methanol
        source: https://webbook.nist.gov/cgi/cbook.cgi?ID=C67561&Mask=4#Thermo-Phase
    """
    if P >= 1.7091124656074195 and P <= 74.70269554659326:        
        A=5.15853
        B=1569.613
        C=-34.846
    
#    P1 = 1.7091124656074195 == 353.5 K
#    P2 = 1.92182184609343 == 356.83 K
#    P3 = 74.70269554659326 == 512.63 K
        return B/(A-np.log10(P))-C
    
    elif P >= 0.09840393278343013 and P < 1.7091124656074195:        

        A=5.20409
        B=1581.341
        C=-33.50
    
#    P0 = 0.09840393278343013 == 288.1 K
#    P1 = 1.829783378587811 == 353.5 K
#    P2 = 2.0572824525040483 == 356.83 K
        
        return B/(A-np.log10(P))-C
    
    else:
        return 513

def evap_MeOH(P):
    """
        calculate the heat of evaporation as function of the temperature for methanol
        source: https://webbook.nist.gov/cgi/cbook.cgi?ID=C67561&Mask=4#Thermo-Phase
    """
    A = 45.3
    alpha = -0.31
    beta = 0.4241
    Tc = 512.6
    Tr = evap_T_MeOH(P) / Tc
    
    print(evap_T_MeOH(P),Tr)
    
    if Tr < 1 and Tr > 0.5620366757705814:
    
        ## ΔvapH = A exp(-αTr) (1 − Tr)**β within 298. - 477. in (kJ/mol)
        ## A = 45.3; α = -0.31, β=0.4241, Tc = 512.6
        
        return A*np.exp(-alpha*Tr)*(1-Tr)**beta
    else:
        return 0.0

#
#def get_minP(P):
#    
#    return abs(evap_T_MeOH2(P)-288.1)
##    return abs(evap_T_MeOH2(P)-evap_T_MeOH1(P))


#solH = optimize.minimize_scalar(evap_T_MeOH2,bounds=[2.0572824525040483,2.1],method="bounded")

#solH = optimize.minimize_scalar(get_minP,bounds=[.000001,84],method="bounded")

#print(solH.x,evap_T_MeOH2(solH.x),evap_T_MeOH(solH.x),evap_T_MeOH2(solH.x)-evap_T_MeOH(solH.x))

print(evap_MeOH(0.0985),evap_MeOH(1),evap_MeOH(10),evap_MeOH(25),evap_MeOH(74))


#minimize_scalar(self.get_residual_heat,bracket=[self.Temperature-50, self.Temperature+40],bounds=[273,400],method="bounded")

    ## ΔvapH = A exp(-αTr) (1 − Tr)**β within 298. - 477. in (kJ/mol)
    ## A = 45.3; α = -0.31, β=0.4241, Tc = 512.6
    
    ## log10(P) = A − (B / (T + C))
    ## Delta T: 353.5 - 512.63; A=5.15853; B=1569.613; C=-34.846
    ## Delta T: 288.1 - 356.83; A=5.20409; B=1581.341; C=-33.50
