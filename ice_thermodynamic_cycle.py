#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  Copyright (c) 2022 Oliver Borm.
#  
#  This program is free software: you can redistribute it and/or modify  
#  it under the terms of the GNU General Public License as published by  
#  the Free Software Foundation, version 3.
# 
#  This program is distributed in the hope that it will be useful, but 
#  WITHOUT ANY WARRANTY; without even the implied warranty of 
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
#  General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License 
#  along with this program. If not, see <http://www.gnu.org/licenses/>.
#
"""
Calculate the thermodynamic cycle of an internal combustion engine. Using two
fuel types:
    - Methanol
    - Syngas

Consider the heat of evaporation for methanol, either before or after the
compression step.

Consider an isentropic efficiency for compression and expansion step.

Requires: cantera >= 2.5.0, matplotlib >= 2.0, scipy >= 0.19, numpy
Keywords: equilibrium
"""

import cantera as ct
import numpy as np

from scipy import optimize
import matplotlib.pyplot as plt

##############################################################################


class cycle:
    def __init__(self):
        # phases
        self.gas = ct.Solution('gri30.yaml')
        #carbon = ct.Solution('graphite.yaml')
        self.T1 = 288.15
        self.T2 = 500.
        self.T3 = 2000.
        self.T4 = 900.
        self.p1 = ct.one_atm
        self.p2 = ct.one_atm*10
        self.p3 = ct.one_atm*10
        
        self.phi = 1.0
        
        # gaseous fuel species
#        self.fuel_species = 'CO:1,H2:2'
        self.fuel_species = 'CH3OH'
        
        self.r = 10
        self.isen_eff = 0.9

    def presure_ratio(self,r):
        """Returns pressure ratio and pressure"""
        
        kappa = self.gas.cp_mass/self.gas.cv_mass
#        print("kappa",kappa)
        
        p2 = self.gas.P*(r)**kappa
        
        return p2/self.gas.P, p2
    
    def isentropic_t_v(self,r,kappa):
        """Returns isentropic temperature as function of density ratio"""
        
        return self.gas.T*(r)**(kappa-1.0)
    
    def isentropic_t(self,pi):
        """Returns isentropic temperature"""
        
        kappa = self.gas.cp_mass/self.gas.cv_mass
#        print("kappa",kappa)
        
        return self.gas.T*(pi)**((kappa-1.0)/kappa)
    
    def get_cp_T_V(self,T2,cpT2star,cpT1,isen_eff,p2):
    
        self.gas.TP = T2,p2
        rhs = (cpT2star/isen_eff+cpT1*(1.-1./isen_eff))
        return self.gas.cp_mass*self.gas.T - rhs
    
    def set_T2(self,T2star,T1,cp1,isen_eff,p2):
        """Returns temperature after isentropic compression"""
        self.gas.TP = T2star, p2
        cpT2star = self.gas.cp_mass*T2star
        
        sol = optimize.root_scalar(self.get_cp_T_V,args=(cpT2star,cp1*T1,isen_eff,p2),x0=T2star,x1=1.1*T2star)#,bracket=[0.001, 0.5],method='bisect')
        return sol.root
    
    def get_cp_T_E(self,T4,cpT4star,cpT3,isen_eff,p4):
    
        self.gas.TP = T4,p4
        rhs = (isen_eff*cpT4star+cpT3*(1.-isen_eff))
        return self.gas.cp_mass*self.gas.T - rhs
    
    def set_T4(self,T4star,T3,cp3,isen_eff,p4):
        """Returns temperature after isentropic compression"""
#        self.gas.TP = T4star, p4
        cpT4star = T4star*cp3 #self.gas.cp_mass*
        
        sol = optimize.root_scalar(self.get_cp_T_V,args=(cpT4star,cp3*T3,isen_eff,p4),x0=T4star,x1=1.1*T4star)#,bracket=[0.001, 0.5],method='bisect')
        return sol.root
    
    def get_cv_T_q(self,T2,cvT1,d1,q):
        
        self.gas.TD = T2,d1
        
        return self.gas.cv_mass*self.gas.T-cvT1-q
    
    def set_injection(self):
        """Set T and density after fuel injection"""
        
        d0 = self.gas.density
        kappa = self.gas.cp_mass/self.gas.cv_mass
        
        self.gas.set_equivalence_ratio(self.phi, self.fuel_species, 'O2:1.0, N2:3.76')
                
        Y_fuel = 0.0
        fuelc = self.fuel_species.split(",")
        for f in fuelc:
            Y_fuel += self.gas[f.split(":")[0]].Y[0]
            
        d1 = d0*(1+(Y_fuel/(1-Y_fuel)))
        
        t1 = self.isentropic_t_v(d1/d0,kappa)
        
        self.gas.TD = t1, d1
        
        return d1, t1
    
    def set_Tdash(self,T1,cv1,q):
        """Returns temperature after fuel evaporation"""
        
        self.gas.set_equivalence_ratio(self.phi, self.fuel_species, 'O2:1.0, N2:3.76')
        
        Y_fuel = 0.0
        fuelc = self.fuel_species.split(",")
        for f in fuelc:
            Y_fuel += self.gas[f.split(":")[0]].Y[0]
        
        d1, t1 = self.set_injection()
        
        sol = optimize.root_scalar(self.get_cv_T_q,args=(cv1*t1,d1,q*Y_fuel),x0=t1,x1=0.1*t1)#,bracket=[0.001, 0.5],method='bisect')
        return sol.root
        
    def adiabatic_t(self,phi,fuel,T,P):
        """Returns the adiabatic temperature for the specified fuel"""
        # set the gas state
        self.gas.set_equivalence_ratio(phi, fuel, 'O2:1.0, N2:3.76')
    
        self.gas.TP = T, P
       
        # equilibrate the mixture adiabatically at constant Volume
        self.gas.equilibrate('UV', solver='auto', max_steps=1000)
    
#        print('cv = {0:.4g} [J/(kg K)]'.format(self.gas.cv_mass))
    
        return self.gas.T, self.gas.P, self.gas.cp_mass, self.gas.cv_mass, self.gas.entropy_mass

    def calculate_cycle(self,evap,qevap):
        """Calculate the cycle"""
        self.gas.TPX = self.T1, self.p1, 'O2:1.0, N2:3.76'
        cp1 = self.gas.cp_mass
        cv1 = self.gas.cv_mass
        s1 = self.gas.entropy_mass
        d0 = self.gas.density
        
        if evap == 0:
            #Verdampfung
            T1dash = self.set_Tdash(self.T1,cv1,qevap)
            cp1 = self.gas.cp_mass
            t1 = T1dash
        elif evap == 2:
            d1, t1 = self.set_injection()
            cp1 = self.gas.cp_mass
            cv1 = self.gas.cv_mass
        else:
            t1 = self.T1
        
        sdash = self.gas.entropy_mass
        
        ## compression
        pi, self.p2 = self.presure_ratio(self.r)
#        self.gas.TDX = t1, d1, 'O2:1.0, N2:3.76'
        
        print("T1,T,t1",self.T1,self.gas.T,t1)
        print("p1,p,p2",self.p1,self.gas.P,self.p2)
        print("d0,density",d0,self.gas.density)
        print("s1,sdash,s",s1,sdash,self.gas.entropy_mass)
        print("cv1,cv",cv1,self.gas.cv_mass)
        
        T2star = self.isentropic_t_v(self.r,self.gas.cp_mass/self.gas.cv_mass)
        self.T2 = self.set_T2(T2star,t1,cp1,self.isen_eff,self.p2)
        cp2 = self.gas.cp_mass
        cv2 = self.gas.cv_mass
        s2 = self.gas.entropy_mass
        
        if evap == 1:
            #Verdampfung
            T2dash = self.set_Tdash(self.T2,cv2,qevap)
            t2 = T2dash
            sdash = self.gas.entropy_mass
        else:
            t2 = self.T2
            
        ## combustion
        self.T3, self.p3, cp3, cv3, s3 = self.adiabatic_t(self.phi,self.fuel_species,t2,self.p2)
        
#        print(self.gas.Y)
        
        ## expansion
        T4star = self.isentropic_t(self.p1/self.p3)
        self.T4 = self.set_T4(T4star,self.T3,cp3,self.isen_eff,self.p1)
        cp4 = self.gas.cp_mass
        s4 = self.gas.entropy_mass
        
        ##
        self.work_compression = cp2*self.T2-cp1*t1
        self.work_expansion = cp4*self.T4-cp3*self.T3
        self.heat_combustion = cv3*self.T3-cv2*t2
        
        ## fill
#        print(pi, 1/(mC1.p1/mC1.p3), mC1.p2, mC1.p3, mC1.T1,t1,t2,T2star,mC1.T2,mC1.T3,T4star,mC1.T4)

#        print
        if evap == 0:
            self.entropies = np.array([s1,sdash,s2,s3,s4])
            self.temperatures = np.array([self.T1,t1,self.T2,self.T3,self.T4])
        elif evap == 1:
            self.entropies = np.array([s1,s2,sdash,s3,s4])
            self.temperatures = np.array([self.T1,self.T2,t2,self.T3,self.T4])
        elif evap == 2:
            self.entropies = np.array([s1,sdash,s2,s3,s4])
            self.temperatures = np.array([self.T1,t1,self.T2,self.T3,self.T4])
            ## take into account onboard reforming
            self.heat_combustion = (cv3*self.T3-cv2*t2)/1.2
        
        
        self.calculate_efficiency()

    def calculate_efficiency(self):
        """Calculate the cycle"""
        self.eff = -(self.work_compression+self.work_expansion)/self.heat_combustion
        

mC1 = cycle()

mC1.isen_eff = 0.9
mC1.r = 10.

evap = 1
fuel = 0

if fuel == 0:
    mC1.fuel_species = 'CH3OH'
    qevap = -1165e3 #[J/kg]
elif fuel == 1:
    mC1.fuel_species = 'CO:1,H2:2'
    qevap = 0.0
    evap = 2

mC1.calculate_cycle(evap,qevap)

print("fuel", fuel, "evap", evap)

print(mC1.work_compression,mC1.work_expansion,mC1.heat_combustion)

print(mC1.eff)

print(mC1.temperatures)
print(mC1.entropies)

plt.plot(mC1.entropies,mC1.temperatures)

plt.show()

#gas.set_equivalence_ratio(1.0, fuel_species, 'O2:1.0, N2:3.76')
#
#pi, p2 = presure_ratio(10)
#T2star = isentropic_t(pi)
#
#print(pi, p2,gas.P, gas.T,T2star)
#
#tad = mC1.adiabatic_t(1.0,fuel_species,T1,p1)
#print('phi = {0:.4g}, Tad = {1:.4g} [K], T1 = {2:.4g} [K], p1 = {3:.4g} [Pa]'.format(1.0, tad,T1,p1))

## equivalence ratio range
#npoints = 51
#phi = np.linspace(0.3, 3.5, npoints)
#
## create some arrays to hold the data
#tad = np.zeros(npoints)
#xeq = np.zeros((mix.n_species, npoints))
#
#for i in range(npoints):
#    tad[i] = adiabatic_t(phi[i],fuel_species,T,P*60)
#    print('At phi = {0:12.4g}, Tad = {1:12.4g}'.format(phi[i], tad[i]))
#    xeq[:, i] = mix.species_moles
#
## write output CSV file for importing into Excel
#csv_file = 'adiabatic.csv'
#with open(csv_file, 'w', newline='') as outfile:
#    writer = csv.writer(outfile)
#    writer.writerow(['phi', 'T (K)'] + mix.species_names)
#    for i in range(npoints):
#        writer.writerow([phi[i], tad[i]] + list(xeq[:, i]))
#print('Output written to {0}'.format(csv_file))
#
#if '--plot' in sys.argv:
#    import matplotlib.pyplot as plt
#    plt.plot(phi, tad)
#    plt.xlabel('Equivalence ratio')
#    plt.ylabel('Adiabatic flame temperature [K]')
#    plt.show()
