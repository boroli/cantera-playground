#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  Copyright (c) 2022 Oliver Borm.
#  
#  This program is free software: you can redistribute it and/or modify  
#  it under the terms of the GNU General Public License as published by  
#  the Free Software Foundation, version 3.
# 
#  This program is distributed in the hope that it will be useful, but 
#  WITHOUT ANY WARRANTY; without even the implied warranty of 
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
#  General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License 
#  along with this program. If not, see <http://www.gnu.org/licenses/>.
#
"""
Calculates the lower heating value and higher heating value for various fuels
including gas mixturess based on their thermodynamic properties. Inspiered by:

https://cantera.org/examples/jupyter/thermo/heating_value.ipynb.html
https://cantera.org/examples/jupyter/thermo/flame_temperature.ipynb.html

WARNING: The LHV for methanol is 21.104 and as such higher than the tabulated
one which is only 19.9.
The LHV for Silane (SiH4) is 25.637 and only half as calculated from tabulated
standard formation enthalpies (https://en.wikipedia.org/wiki/Silane#Safety_and_precautions).
I think the algorithm is correct, but maybe the underlying thermodynamic data isn't?!?

Requires: cantera >= 2.5.0, reportlab (only for pdf export)
Keywords: heating value
"""

import cantera as ct

##############################################################################

pdfexport = False
# add Disilan and Trisilan from the silane.yaml
add_silane = False

water = ct.Water()
# Set liquid water state, with vapor fraction x = 0
water.TQ = 298, 0
h_liquid = water.h
# Set gaseous water state, with vapor fraction x = 1
water.TQ = 298, 1
h_gas = water.h

def heating_value(fuel):
    """Returns the LHV and HHV for the specified fuel"""
    
    mspecs = ['O2', 'N2','CO2', 'H2O', 'SiO2']
    
    species = {S.name: S for S in ct.Species.list_from_file('nasa_gas.yaml')}
    complete_species = [species[S] for S in (mspecs)]
    
    if ("SI2H6".lower() in fuel.lower()) or ("SI3H8".lower() in fuel.lower()) :
        si_species = {S.name: S for S in ct.Species.list_from_file('silane.yaml')}
#        print(si_species)
        fuelc = fuel.split(",")
        for f in fuelc:
            complete_species.append(si_species[f.split(":")[0]])
    else:
        fuelc = fuel.split(",")
        for f in fuelc:
            complete_species.append(species[f.split(":")[0]])
    
#    print(complete_species)
     
    gas = ct.Solution(thermo='IdealGas', species=complete_species)
    
    gas.TP = 298.15, ct.one_atm
    gas.set_equivalence_ratio(1.0, fuel, "O2:1.0")
    
    ## The mixture fraction doesn't take into account Si to be oxidised: https://cantera.org/documentation/dev/sphinx/html/cython/thermo.html#cantera.ThermoPhase.set_equivalence_ratio
    ## thus we need to set the stoichiometry by hand for all silane
    if fuel == "SiH4":
        gas.TPX = 298.15, ct.one_atm, "SiH4:1.,O2:2."
    elif fuel == "SI2H6":
        gas.TPX = 298.15, ct.one_atm, "SI2H6:1.,O2:3.5"
    elif fuel == "SI3H8":
        gas.TPX = 298.15, ct.one_atm, "SI3H8:1.,O2:5.0"

    h1 = gas.enthalpy_mass
    h1mol = gas.enthalpy_mole
    XO2 = gas['O2'].X[0]
    
    X_fuel = 0.0
    Y_fuel = 0.0
    for f in fuelc:
        X_fuel += gas[f.split(":")[0]].X[0]
        Y_fuel += gas[f.split(":")[0]].Y[0]
        
    # complete combustion products
    X_products = {
        "CO2": gas.elemental_mole_fraction("C"),
        "H2O": 0.5 * gas.elemental_mole_fraction("H"),
        "N2": 0.5 * gas.elemental_mole_fraction("N"),
        "SiO2": gas.elemental_mole_fraction("Si")
    }
    
#    print(fuel,X_fuel,gas['O2'].X[0],gas['O2'].X[0]/X_fuel,Y_fuel)

    gas.TPX = None, None, X_products
    Y_H2O = gas["H2O"].Y[0]
    h2 = gas.enthalpy_mass
    h2mol = gas.enthalpy_mole
    
    LHV = -(h2 - h1) / Y_fuel / 1e6
    HHV = -(h2 - h1 + (h_liquid - h_gas) * Y_H2O) / Y_fuel / 1e6
    
#    print(fuel,X_products,sum(X_products.values()))
#    print(fuel,X_fuel*3,XO2*3,gas['SiO2'].X[0]*3,gas['H2O'].X[0]*3)
#    print("h1mol",h1mol,h2mol,-(h2mol-h1mol)*1e-6*3)  
    
    return LHV, HHV
#
#fuel = "CH3OH" #:1,H2:2"
#LHV, HHV = heating_value(fuel)
#print(f"{fuel:8s} {LHV:7.3f}      {HHV:7.3f}")

fuels = ["H2", "CH4", "C2H6", "C3H8", "NH3", "CH3OH", "CO", "CO:1,H2:2", "N2H4", "SiH4"]

if add_silane:
    fuels.append("SI2H6")
    fuels.append("SI3H8")

outputLines = []

outputLines.append("fuel   LHV (MJ/kg)   HHV (MJ/kg)")
for fuel in fuels:
    LHV, HHV = heating_value(fuel)
    outputLines.append(f"{fuel:8s} {LHV:7.3f}      {HHV:7.3f}")

## print global values
print(*outputLines, sep = "\n")

if pdfexport:
    from reportlab.pdfgen import canvas
    from reportlab.lib.units import cm
    
    c = canvas.Canvas("Heating_Values.pdf")
     
    # move the origin up and to the left
    c.translate(cm,cm)
    c.setFont("Vera", 10)
    textobject = c.beginText(0, 750)
    for line in outputLines:
        textobject.textLine(line)
     
    c.drawText(textobject)
    c.save()
    
