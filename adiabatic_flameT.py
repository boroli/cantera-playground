#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  Copyright (c) 2022 Oliver Borm.
#  
#  This program is free software: you can redistribute it and/or modify  
#  it under the terms of the GNU General Public License as published by  
#  the Free Software Foundation, version 3.
# 
#  This program is distributed in the hope that it will be useful, but 
#  WITHOUT ANY WARRANTY; without even the implied warranty of 
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
#  General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License 
#  along with this program. If not, see <http://www.gnu.org/licenses/>.
#
"""
Adiabatic flame temperature and equilibrium composition for a fuel/air mixture.

Calculate the the adiabatic flame temperature analytically by integrating cp(T)
and cv(T) over T, and compare it to the chemical equilibrium obtained
numerically.

Compare the adiabatic flame temperature for constant volume and constant
pressure combustion.

Requires: cantera >= 2.5.0, matplotlib >= 2.0, scipy >= 0.19
Keywords: equilibrium, combustion
"""

import cantera as ct
from scipy.integrate import quad
from scipy import optimize

phi = 1.0
fuel = "CH3OH"
Tref = 298.15
Tu = 900

## use all species of GRI
#gas = ct.Solution('gri30.yaml')

## only use minimal species
species = {S.name: S for S in ct.Species.list_from_file('nasa_gas.yaml')}   
mspecs = ['O2', 'N2','CO2', 'H2O']
fuelc = fuel.split(",")
for f in fuelc:
    mspecs.append(f.split(":")[0])

complete_species = [species[S] for S in (mspecs)]
gas = ct.Solution(thermo='IdealGas', species=complete_species)

gas.TPX = Tref, ct.one_atm, 'O2:1.0, N2:3.76'

def cp_T(t):
    gas.TP = t, gas.P
    return gas.cp_mass ##[J/kg/K]

def cv_T(t):
    gas.TD = t, gas.density
    return gas.cv_mass ##[J/kg/K]


gas.set_equivalence_ratio(phi, fuel, 'O2:1.0, N2:3.76')
gas.TP = Tref, ct.one_atm

Yu = gas.Y
hu = gas.enthalpy_mass
uu = gas.int_energy_mass # Specific internal energy [J/kg]
Mu = gas.mean_molecular_weight #[kg/kmol]
Ru = ct.gas_constant/Mu ## The ideal gas constant, J kmol-1 K-1

cpTu = quad(cp_T,Tref,Tu)[0]
cvTu = quad(cv_T,Tref,Tu)[0]

#print("Yu", Yu, "hu", hu,"cpTu",cpTu)

# complete combustion products
X_products = {
    "CO2": gas.elemental_mole_fraction("C"),
    "H2O": 0.5 * gas.elemental_mole_fraction("H"),
    "N2": 0.5 * gas.elemental_mole_fraction("N"),
}
gas.TPX = Tref, ct.one_atm, X_products

Yb = gas.Y
hb = gas.enthalpy_mass
ub = gas.int_energy_mass # Specific internal energy [J/kg]
Mb = gas.mean_molecular_weight #[kg/kmol]
Rb = ct.gas_constant/Mu ## The ideal gas constant, J kmol-1 K-1

#print("Yb", Yb, "hb", hb)

deltah = hu-hb
deltau = uu-ub

def Tad(t):
    return quad(cp_T,Tref,t)[0]-cpTu-deltah
sol = optimize.root_scalar(Tad,x0=1700,x1=2000)

Tad = sol.root

def Tad_v(t):
    return quad(cv_T,Tref,t)[0]-cvTu-deltau
sol1 = optimize.root_scalar(Tad_v,x0=1700,x1=2000)
Tadv = sol1.root

gas.set_equivalence_ratio(phi, fuel, 'O2:1.0, N2:3.76')
gas.TP = Tu, ct.one_atm
gas.equilibrate('HP', solver='auto', max_steps=1000)
Teq = gas.T

gas.set_equivalence_ratio(phi, fuel, 'O2:1.0, N2:3.76')
gas.TP = Tu, ct.one_atm
gas.equilibrate('UV', solver='auto', max_steps=1000)
Teq_v = gas.T

print(Tad,Tadv)
print(Teq,Teq_v)

